module PgAdvisoryLock
  class Base

    class << self
      def synchronise(sym, options = {}, &blk)
        connection = options[:connection] || ActiveRecord::Base.connection
        instance_id = options[:instance_id]

        id = sym
        id = PgAdvisoryLock.id_for_symbol(sym) if id.is_a?(Symbol)
        raise InvalidKey.new("Symbol undefined. Please update the #{self.to_s} symbol table.") unless id
        ids = [id]
        ids << instance_id if instance_id

        results = nil
        lock(connection, options, ids) do
          results = yield if block_given?
        end

        results
      end
      alias_method :synchronize, :synchronise

      def lock(connection, options, ids, &blk)
        raise NotImplementedError
      end
      protected :lock

    end
  end
end
