require 'pg_advisory_lock/base'

module PgAdvisoryLock
  class XactLock < Base

    class << self
      def lock(connection, options, ids, &blk)
        with_transaction(connection) do
          if try_lock(connection, options[:shared], ids)
            results = yield if block_given?
          else
            raise LockUnavailable.new("#{self.to_s} not available")
          end
        end
      end

      def lock_function(shared)
        shared ? "pg_try_advisory_xact_lock_shared" : "pg_try_advisory_xact_lock"
      end

      def with_transaction(connection)
        if connection.outside_transaction?
          connection.transaction do
            results = yield if block_given?
          end
        else
          results = yield if block_given?
        end
        results
      end
      protected :with_transaction

      def try_lock(connection, shared, ids)
        command = "SELECT #{lock_function(shared)}(#{ids.collect{|i| i.to_i.to_s}.join(', ')})"
        result = connection.select_value(command)
        Boolean.parse(result)
      end
      protected :try_lock

    end
  end
end
