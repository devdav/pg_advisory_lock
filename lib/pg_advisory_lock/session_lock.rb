require 'pg_advisory_lock/base'

module PgAdvisoryLock
  class SessionLock < Base

    class << self
      def lock(connection, options, ids, &blk)
        if try_lock(connection, options[:shared], ids)
          begin
            results = yield if block_given?
          ensure
            unlocked = unlock(connection, options[:shared], ids)
            PgAdvisoryLock.logger.error{"#{self.to_s} unable to unlock"} unless unlocked
          end
        else
          raise LockUnavailable.new("#{self.to_s} not available")
        end
      end

      def lock_function(shared)
        shared ? "pg_advisory_xact_lock_shared" : "pg_try_advisory_lock"
      end
      def unlock_function(shared)
        shared ? "pg_advisory_unlock_shared" : "pg_advisory_unlock"
      end
      
      def try_lock(connection, shared, ids)
        command = "SELECT #{lock_function(shared)}(#{ids.collect{|i| i.to_i.to_s}.join(', ')})"
        result = connection.select_value(command)
        Boolean.parse(result)
      end
      def unlock(connection, shared, ids)
        command = "SELECT #{unlock_function(shared)}(#{ids.collect{|i| i.to_i.to_s}.join(', ')})"
        result = connection.select_value(command)
        Boolean.parse(result)
      end
      protected :try_lock, :unlock

    end
  end
end
