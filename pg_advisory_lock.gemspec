# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "pg_advisory_lock/version"

Gem::Specification.new do |s|
  s.name        = "pg_advisory_lock"
  s.version     = PgAdvisoryLock::VERSION
  s.authors     = ["Dan Sketcher"]
  s.email       = ["dan.sketcher@paymenthub.com.au"]
  s.homepage    = "https://bitbucket.org/paymenthub/pg_advisory_lock"
  s.summary     = "A wrapper for PostgreSQL Advisory Locks"
  s.description = %Q(A quick, reliable way of using pg_advisory_lock and friends to obtain and release locks.
Particularly useful for cron jobs and long running tasks that must not be run with more than one instance concurrently.
)

  s.rubyforge_project = "pg_advisory_lock"

  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ["lib"]

  # specify any dependencies here; for example:
  s.add_development_dependency "rspec", "~> 2.5"
  s.add_runtime_dependency "pg", ">= 0.11.0"
  s.add_runtime_dependency "activerecord"
end
