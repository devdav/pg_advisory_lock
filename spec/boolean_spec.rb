require 'spec_helper'

describe PgAdvisoryLock::Boolean do
  it 'parses PostgreSQL style raw booleans to Ruby booleans' do
    PgAdvisoryLock::Boolean.parse('t').should eq true
    PgAdvisoryLock::Boolean.parse('T').should eq false # does come upcased from postgresql
    PgAdvisoryLock::Boolean.parse('f').should eq false
    PgAdvisoryLock::Boolean.parse('F').should eq false
    PgAdvisoryLock::Boolean.parse('').should eq false
    PgAdvisoryLock::Boolean.parse(nil).should eq false
  end
end
