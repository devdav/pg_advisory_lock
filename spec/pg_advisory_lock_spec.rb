require 'spec_helper'

describe PgAdvisoryLock do
  it 'should return correct version string' do
    PgAdvisoryLock.version_string.should == PgAdvisoryLock::VERSION
  end

  it "gets and sets a logger" do
    # Check we do not keep making new loggers
    PgAdvisoryLock.logger.should_not be_nil
    PgAdvisoryLock.logger.class.should eq Logger
    first_instance = PgAdvisoryLock.logger
    PgAdvisoryLock.logger.object_id.should == first_instance.object_id

    logger = Logger.new('base.log')
    PgAdvisoryLock.logger = logger
    PgAdvisoryLock.logger.object_id.should == logger.object_id
  end

  it "manages symbols" do
    PgAdvisoryLock.symbols = []
    PgAdvisoryLock.symbol_for_id(100).should == nil
    PgAdvisoryLock.symbols = :test1
    PgAdvisoryLock.symbol_for_id(100).should == :test1
    PgAdvisoryLock.symbols = :test1, :test2
    PgAdvisoryLock.symbol_for_id(100).should == :test1
    PgAdvisoryLock.symbol_for_id(101).should == :test2

    PgAdvisoryLock.symbols.should == [:test1, :test2]
  end

  it "manages offsets" do
    PgAdvisoryLock.symbols = []
    PgAdvisoryLock.symbol_for_id(100).should == nil
    PgAdvisoryLock.symbols = :test1
    PgAdvisoryLock.symbol_for_id(100).should == :test1
    PgAdvisoryLock.symbols = :test1, :test2
    PgAdvisoryLock.symbol_for_id(100).should == :test1
    PgAdvisoryLock.symbol_for_id(101).should == :test2

    PgAdvisoryLock.offset = 300

    PgAdvisoryLock.symbols = []
    PgAdvisoryLock.symbol_for_id(300).should == nil
    PgAdvisoryLock.symbols = :test1
    PgAdvisoryLock.symbol_for_id(300).should == :test1
    PgAdvisoryLock.symbols = :test1, :test2
    PgAdvisoryLock.symbol_for_id(300).should == :test1
    PgAdvisoryLock.symbol_for_id(301).should == :test2
  end

  it "allows choosing lock type" do
    PgAdvisoryLock.locktype.should eq (PgAdvisoryLock.server_version > 901000) ? PgAdvisoryLock::XactLock : PgAdvisoryLock::SessionLock

    PgAdvisoryLock.lock_with(:xact_lock)
    PgAdvisoryLock.locktype.should eq PgAdvisoryLock::XactLock

    PgAdvisoryLock.lock_with(:session_lock)
    PgAdvisoryLock.locktype.should eq PgAdvisoryLock::SessionLock

    PgAdvisoryLock.lock_with(:xact_lock)
    PgAdvisoryLock.locktype.should eq PgAdvisoryLock::XactLock
  end

  it "synchronises" do
    this = 0
    this = PgAdvisoryLock.synchronise(1) do
      1
    end
    this.should eq 1
  end

  it "reports that a lock has been acquired" do
    PgAdvisoryLock.symbols = :test1, :test2

    PgAdvisoryLock.synchronise(:test1) do
      PgAdvisoryLock.locked?(:test1).should == true
      PgAdvisoryLock.locked?(:test2).should == false
    end
  end

  it "reports that a lock has been acquired with an instance_id" do
    PgAdvisoryLock.symbols = :test1, :test2

    PgAdvisoryLock.synchronise(:test1, :instance_id => 1) do
      PgAdvisoryLock.locked?(:test1, :instance_id => 1).should == true
      PgAdvisoryLock.locked?(:test1, :instance_id => 2).should == false
      PgAdvisoryLock.locked?(:test1).should == true
      PgAdvisoryLock.locked?(:test2).should == false
    end
  end
end
